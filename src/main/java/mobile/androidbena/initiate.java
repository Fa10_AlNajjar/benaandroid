package mobile.androidbena;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeMethod;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
public class initiate {
    public static AndroidDriver driver;
    public WebDriver getDriver() {
        return driver;
    }
    @BeforeMethod

    public void setup() throws MalformedURLException, InterruptedException {
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("automationName", "UiAutomator2");
        dc.setCapability("platformName", "Android");
        dc.setCapability("platformVersion", "9");
        dc.setCapability("deviceName", "Galaxy A10");
        dc.setCapability("app", "C:\\Users\\NadSoft\\Downloads\\app-debug.apk");
        dc.setCapability("appActivity", ".ui.splash.SplashActivity");
        dc.setCapability("appPackage", "net.nadsoft.bena");
        URL remoteUrl = new URL("http://localhost:4723/wd/hub");
        driver = new AndroidDriver(remoteUrl, dc);
        Thread.sleep(5000);
    }
    public static void sendKeys(String value, By locator) throws InterruptedException {
        Random rand = new Random();
        int rnd = rand.nextInt(999) + 1;
        if (value.contains("[random]")) {
            value = value.replace("[random]", rnd + String.valueOf(System.currentTimeMillis()));
        }}
        public static void scroll() throws InterruptedException {
            Dimension size = driver.manage().window().getSize();
            int starty = (int) (size.height * 0.80);
            int endy = (int) (size.height * 0.20);
            int startx = size.width / 2;
            System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);

            driver.swipe(startx, starty, startx, endy, 3000);
            Thread.sleep(2000);
            driver.swipe(startx, endy, startx, starty, 3000);
        }

}