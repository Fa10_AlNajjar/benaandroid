package Locators;

import org.openqa.selenium.By;

public class Login_Bena_Locators {
    public static By Login_Password = By.id("net.nadsoft.bena:id/passwordTxt");
    public static By Forget_Password = By.id("net.nadsoft.bena:id/forgetPasswordBtn");
    public static By Login = By.id("net.nadsoft.bena:id/loginBtn");
    public static By Register_from_Login = By.id("net.nadsoft.bena:id/registerTxtBtn");
    public static By Skip = By.id("net.nadsoft.bena:id/skipBtn");
    public static By Send_Verify_Number_Password = By.id("net.nadsoft.bena:id/sendPasswordBtn");
    public static By resend_VerifyNo_Again = By.id("");
    public static By MapPremision = By.id("com.android.packageinstaller:id/permission_message");
    public static By Menu = By.id("net.nadsoft.bena:id/menuBtn");
    public static By TermsCondition = By.id("net.nadsoft.bena:id/bottomgradient");
    public static By Deny = By.id("com.android.packageinstaller:id/permission_deny_button");
    public static By Allow = By.id("com.android.packageinstaller:id/permission_allow_button");
    public static By Yes_After_Logout = By.id("net.nadsoft.bena:id/yesBtn");
    public static By No_After_Logout = By.id("net.nadsoft.bena:id/noBtn");
}
