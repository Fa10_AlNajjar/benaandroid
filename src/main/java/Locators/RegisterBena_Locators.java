package Locators;

import org.openqa.selenium.By;

public class RegisterBena_Locators {
    public static By creatnew_account = By.id("net.nadsoft.bena:id/registerTxtBtn");
    public static By username = By.id("net.nadsoft.bena:id/usernameTxt");
    public static By Phone_no = By.id("net.nadsoft.bena:id/phoneTxt");
    public static By Email = By.id("net.nadsoft.bena:id/emailTxt");
    public static By Password = By.id("net.nadsoft.bena:id/passwordTxt");
    public static By Reconfirm_Password = By.id("net.nadsoft.bena:id/confirmPasswordTxt");
    public static By Create_Account = By.id("net.nadsoft.bena:id/createAccountBtn");
    public static By You_Have_Account_Already = By.id("net.nadsoft.bena:id/loginBtn");
    public static By Verify_Number = By.id("");
    public static By Submit_Verify = By.id("");
    public static By Resend_TheVerification_Number = By.id("");
    public static By Submit = By.id("net.nadsoft.bena:id/createAccountBtn");
}
