package Actions;
import Locators.Login_Bena_Locators;

import Locators.RegisterBena_Locators;
import io.appium.java_client.MobileElement;
import mobile.androidbena.initiate;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class LoginTest extends initiate {
    @Test (priority = 1,description = "LoginByValidEmail",testName = "HappyLoginScenario,Login by valid Email")
    public static void Login () throws InterruptedException {
        driver.findElement(RegisterBena_Locators.username).sendKeys("faten+android1@nadsoft.net");
        driver.findElement(Login_Bena_Locators.Login_Password).sendKeys("123456789");
        driver.findElement(Login_Bena_Locators.Login).click();
        Thread.sleep(5000);

        MobileElement x = (MobileElement) driver.findElement(Login_Bena_Locators.MapPremision);
        if (x.isDisplayed()) {
            System.out.println("Login by Email done successfully");
        }
        else {System.out.println("Login by Email failed");
        }

    }
     @Test(priority = 0,description = "LoginByValidPhoneNumber",testName = "HappyScenarioWithPhoneNumber")
    public static void Login_by_phone() throws InterruptedException {
        driver.findElement(RegisterBena_Locators.username).sendKeys("533333333");
        driver.findElement(Login_Bena_Locators.Login_Password).sendKeys("123456789");
        driver.findElement(Login_Bena_Locators.Login).click();
        Thread.sleep(5000);
        MobileElement x = (MobileElement) driver.findElement(Login_Bena_Locators.MapPremision);
        if (x.isDisplayed()) {
            System.out.println("Login by phone number done successfully");
        }
        else {System.out.println("Login by phone number  failed");
        }
    }
    @Test(priority = 2,testName = "LoginUsingIncorrectemail",description = "UnhappyScenario, login by invalid Email")
    public static void Login_By_InvalidEmail () {
        driver.findElement(RegisterBena_Locators.username).sendKeys("abcd@nadsoft.net");
        driver.findElement(Login_Bena_Locators.Login_Password).sendKeys("faten55132");
        driver.findElement(Login_Bena_Locators.Login).click();
        if (driver.findElement(By.id("net.nadsoft.bena:id/titleLabel")).isEnabled()) {
            System.out.println("Login By Invalid Email is passed ");
        } else {System.out.println("Login By Invalid Email is failed ");}
    }
    @Test(priority = 3,testName = "LoginUsingIncorrectPasswprd",description = "UnhappyScenarioLogin")
    public static void Login_By_WrongPassword () {
        driver.findElement(RegisterBena_Locators.username).sendKeys("faten+android1@nadsoft.net");
        driver.findElement(Login_Bena_Locators.Login_Password).sendKeys("faten+55132");
        driver.findElement(Login_Bena_Locators.Login).click();
        if (driver.findElement(By.id("net.nadsoft.bena:id/titleLabel")).isEnabled()) {
            System.out.println("Login By wrong password is passed ");
        } else {System.out.println("Login By wrong password is failed ");}
    }

    @Test(priority = 5,testName = "GoToSignUpFromLoginPage",description = "PressonCreateNewAccountFromLoginScreen")
    public static void GoTo_SignUp_From_Login () {
        driver.findElement(Login_Bena_Locators.Register_from_Login).click();
        MobileElement x = (MobileElement) driver.findElement(By.id("net.nadsoft.bena:id/titleLabel"));
        if (x.isDisplayed()) {
            System.out.println("Open Signup screen from Login has been done successfully");
        }
        else {System.out.println("Open Signup screen from Login has not been done successfully");
        }
    }
  /*  @Test(priority = 6,testName = "ForgetPassword",description = "ForgetPasswordByPhone")
    public static void forget_password () {
        driver.findElement(Login_Bena_Locators.Forget_Password).click();
        driver.findElement(RegisterBena_Locators.username).sendKeys("522222222");
        driver.findElement(Login_Bena_Locators.Send_Verify_Number_Password).click();
        driver.findElement(By.id("net.nadsoft.bena:id/optView")).sendKeys("12345");
    }*/
    @Test(priority = 7,testName = "ResendVerifyNumber",description = "ResendVerifyNumbertoEmail")
    public static void resend_verify_number() {
        driver.findElement(Login_Bena_Locators.Forget_Password).click();
        driver.findElement(RegisterBena_Locators.username).sendKeys("faten+android1@nadsoft.net");
        driver.findElement(Login_Bena_Locators.Send_Verify_Number_Password).click();
       // driver.findElement(Login_Bena_Locators.resend_VerifyNo_Again).click();
    }

}
