package Actions;

import Locators.Login_Bena_Locators;
import Locators.sidemenulocators;
import io.appium.java_client.MobileElement;
import mobile.androidbena.initiate;
import org.apache.http.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GuestUser extends initiate {
    @Test(priority = 4,testName = "EnterToBenaAppAsGuestUser",description = "PressOnSkip,YouAreGuestUser")
    public static void Skip () throws InterruptedException {
        driver.findElement(Login_Bena_Locators.Skip).click();
        Thread.sleep(3000);
        MobileElement x = (MobileElement) driver.findElement(Login_Bena_Locators.MapPremision);
        if (x.isDisplayed()) {
            System.out.println("Skip Done successfully");
        }
        else {System.out.println("Skip has not been done successfully");
        }
        driver.findElement(Login_Bena_Locators.Allow).click();
        Thread.sleep(1000);
        driver.findElement(Login_Bena_Locators.Menu).click();
    }
    @Test (priority = 1,description = "OpenSideMenuForGuestUser",testName = "Open side menu has not be allowed")
    public static void OpenSideMenuForGuestUser () throws InterruptedException {
               driver.findElement(Login_Bena_Locators.Skip).click();
        Thread.sleep(2000);
        driver.findElement(Login_Bena_Locators.Allow).click();
        Thread.sleep(2000);
        driver.findElement(sidemenulocators.menu_btn).click();
        Thread.sleep(2000);
        MobileElement x= (MobileElement) driver.findElement(By.id("net.nadsoft.bena:id/sorry"));
        if (x.isDisplayed()) {
            System.out.println("Guest user cant reach menu");
        }
        else {System.out.println("Guest user can reach side menu");}

    }
    @Test (priority = 2,description = "GoToLoginIfIamGuestUser",testName = "Side menu messag ask me to login")
    public static void Go_To_Login_AsGuestUser () throws InterruptedException {
        driver.findElement(Login_Bena_Locators.Skip).click();
        Thread.sleep(2000);
        driver.findElement(Login_Bena_Locators.Allow).click();
        Thread.sleep(2000);
        driver.findElement(sidemenulocators.menu_btn).click();
        Thread.sleep(4000);
        MobileElement x= (MobileElement) driver.findElement(By.id("net.nadsoft.bena:id/sorry"));
        if (x.isDisplayed()) {
            System.out.println("Guest user cant reach menu");
        }
        else {System.out.println("Guest user can reach side menu");}
        driver.findElement(sidemenulocators.LoginAsGuestUser).click();

        }
        @Test (priority = 3,description = "GoToSignupIfIamGuestUser",testName = "Side menu messag ask me to Register")
        public static void GoToSignUpAsGuestUser () throws InterruptedException {
            driver.findElement(Login_Bena_Locators.Skip).click();
            Thread.sleep(2000);
            driver.findElement(Login_Bena_Locators.Allow).click();
            driver.findElement(sidemenulocators.menu_btn).click();
            Thread.sleep(2000);
            MobileElement x= (MobileElement) driver.findElement(By.id("net.nadsoft.bena:id/sorry"));
            if (x.isDisplayed()) {
                System.out.println("Guest user cant reach menu");
            }
            else {System.out.println("Guest user can reach side menu");}
            driver.findElement(sidemenulocators.RegisterGuestUser).click();
        }

    }

