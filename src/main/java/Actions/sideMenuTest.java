package Actions;

import Locators.Login_Bena_Locators;
import Locators.sidemenulocators;
import mobile.androidbena.initiate;
import org.testng.annotations.Test;

public class sideMenuTest extends initiate {
    @Test
    public static void OpenMyProfile() throws InterruptedException {
        LoginTest.Login();
        driver.findElement(Login_Bena_Locators.Allow).click();
        Thread.sleep(1500);
        driver.findElement(sidemenulocators.menu_btn).click();
        driver.findElement(sidemenulocators.MyProfile).click();
    }
    @Test
    public static void OpenMyProfileFromTheList () throws InterruptedException {
        Thread.sleep(1500);
        LoginTest.Login();
        driver.findElement(Login_Bena_Locators.Allow).click();
        Thread.sleep(1500);
        driver.findElement(sidemenulocators.menu_btn).click();
        Thread.sleep(1500);
        driver.findElement(sidemenulocators.Myprofile2).click();
    }
    @Test
    public static void FilterFromMenu () throws InterruptedException {
        LoginTest.Login();
        driver.findElement(Login_Bena_Locators.Allow).click();
        Thread.sleep(1500);
        driver.findElement(sidemenulocators.menu_btn).click();
        Thread.sleep(1500);
        driver.findElement(sidemenulocators.Ershad_icom).click();
        driver.findElement(sidemenulocators.FeedBtn).click();
        Thread.sleep(1500);
        initiate.scroll();
    }
    @Test
    public static void GoToPrivacyComdition () throws InterruptedException {
        LoginTest.Login();
        driver.findElement(Login_Bena_Locators.Allow).click();
        Thread.sleep(1500);
        driver.findElement(sidemenulocators.menu_btn).click();
        Thread.sleep(1500);
       initiate.scroll();
        Thread.sleep(2000);
        driver.findElement(Login_Bena_Locators.TermsCondition).click();
    }
    @Test
    public static void LogOut() throws InterruptedException {
        LoginTest.Login();
        driver.findElement(Login_Bena_Locators.Allow).click();
        Thread.sleep(1500);
        driver.findElement(sidemenulocators.menu_btn).click();
        Thread.sleep(1500);
        initiate.scroll();
        Thread.sleep(1500);
        driver.findElement(sidemenulocators.Logout).click();
        Thread.sleep(2000);
       driver.findElement(Login_Bena_Locators.Yes_After_Logout).click();
    }
    @Test
    public static void Volunteer1() throws InterruptedException {
        LoginTest.Login();
        driver.findElement(Login_Bena_Locators.Allow).click();
        Thread.sleep(1500);
        driver.findElement(sidemenulocators.menu_btn).click();
        Thread.sleep(1500);
        driver.findElement(sidemenulocators.Ask_volunteer).click();

    }
    @Test
    public static void volunteer2 () throws InterruptedException {
            LoginTest.Login();
            driver.findElement(Login_Bena_Locators.Allow).click();
            Thread.sleep(1500);
            driver.findElement(sidemenulocators.menu_btn).click();
            Thread.sleep(2000);
            driver.findElement(sidemenulocators.wantvolunteer).click();

    }
}
